# POP2 Kernel

This repository systematically classifies parallel algorithms and kernels based on the idea of parallel patterns.

**Authors:**

- *Julian Miller - miller@itc.rwth-aachen.de*
- *Lukas Trümper - truemper@itc.rwth-aachen.de*.

### A Parallel Pattern based Model

The essential idea of our classification approach separates a kernel's structure from its function
$$
parallel \; algorithm \; = \; structure \; + \; function.
$$
Structure describes the kernel's high-level structure of parallelism, like the scheme of synchronization and dependencies. In short, which *parallel patterns* are observable. On the other side, function means the concrete operations, which are performed within the structure. 

For better understanding consider two kernels:

1. The first kernel simply increments each entry of an array:

   ```c
   int[] array;
   ...
   for (int i = 0; i < len(array); i++){
     array[i] += 1
   }
   ```

2. The second kernel computes a linear mapping of the logarithm of each entry of an array:

   ```c
   int[] array;
   ...
   for (int i = 0; i < len(array); i++){
     array[i] = math.log(array[i]) * 2 + 10
   } 
   ```

In our model, 1. and 2. have identical structure but different functions.

#### Classification of Structure

Structure is seen as a high-level property of a parallel algorithm. In particular, structure directly corresponds to the potential degree of parallelism. Hence,

> structure defines the *scalability* of an algorithm.

Since structure expressed by programming languages follows a hierarchical scheme, the classification is also based on hierarchical compositions of parallel patterns. A parallel pattern describes a certain kind of dependency and synchronization. Typically, these patterns describe rather small structures like

- a **map**,
- a **reduction**,
- or communication like a **broadcast**.

However, the hierarchical composition of them allows to describe every larger structure in a simple way. Moreover, previous example implies a crucial property:

> the structure spans *equivalence classes* on the set of parallel algorithms.

The members of the **equivalence classes** share all properties directly affected by the structure and the high-level parallelism. For instance, we would expect the two kernels of previous example expect to have similar scalability and parallelization effort.

#### Classification of Function

Functions are concrete computations performed within a structure. Thus, they actually define the algorithm as they define the output. Classification of computations can be detailed up to any desired degree. Typically, a classification might cover properties

- measuring the computational intensity (**compute-bound**),
  - instruction counts (e.g. number of arithmetic expressions),
- measuring the memory intensity (**memory-bound**),
  - caching behavior,
  - load-to-store ratio,
- measuring the **instruction-level parallelism**,
  - pipelining ability,
  - vector operations,
  - e.g. branching count.
- categorizing the **application**,
  - matrix-vector,
  - graphs.

Thus,

> the functions define the serial (thread-wise) performance.

#### Runtime 

The main advantage of the model is that it allows systematic reasoning about the runtime behavior of a parallel algorithm based on static properties. More particular, since

- the class of the structure defines the scalability,
- the properties of the function define the serial peformance,

the overall performance might be predicted by the combined investigation.

This means, a parallel algorithm scales with increasing core count according to the scaling behavior of its patterns; a map scales differently than a reduction. Together with the serial performance the overall performance can be estimated. Moreover, the serial performance indicates whether the scalability might saturate because of machine limitations. For instance, a memory-bound function limits the scalabiltiy of a map.

Therefore, the measurement of **runtime properties (performance metrics)** would be an essential part of the validiation of the model.

